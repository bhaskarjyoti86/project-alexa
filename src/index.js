var APP_ID = undefined;//replace with 'amzn1.echo-sdk-ams.app.[your-unique-value-here]';

var http = require('http'),
    alexaDateUtil = require('./alexaDateUtil');

var AlexaSkill = require('./AlexaSkill');

var TidePooler = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
TidePooler.prototype = Object.create(AlexaSkill.prototype);
TidePooler.prototype.constructor = TidePooler;

// ----------------------- Override AlexaSkill request and intent handlers -----------------------

TidePooler.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

TidePooler.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleWelcomeRequest(response);
};

TidePooler.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

/**
 * override intentHandlers to map intent handling functions.
 */
TidePooler.prototype.intentHandlers = {
    "OneShotSimpsonIntent": function(intent,session,response){
        if(intent.Time && intent.slots.Time.value){
            handleOneTimeRequest(intent,session,response);
        }
        else{
        handleOnePaisaRequest(intent,session,response);
        }
    },
    "DialogSimpsonIntent" : function(intent,session,response){
            var country = intent.slots.Country;
           // var time = intent.slots.Time;
            if(country && country.value){
                handleCountryDialogRequest(intent,session,response);
            }
            else{
                handleNoSlotDialogRequest(intent,session,response);
            }
    }
    ,
    "SupportedCitiesIntent": function (intent, session, response) {
        handleSupportedCitiesRequest(intent, session, response);
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        handleHelpRequest(response);
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    }
};

// -------------------------- TidePooler Domain Specific Business Logic --------------------------

// example city to NOAA station mapping. Can be found on: http://tidesandcurrents.noaa.gov/map/
var STATIONS = {
    'india': 'IN',
    'france': 'FR',
    'italy': 'IT',
    'china': 'CN',
    'mexico': 'MX',
    'canada': 'CA',
    'morocco': 'MR'
};

var moneyAgents =[
    "xoom",
    "moneygram",
    "western union",
    "remit india"
];

function handleWelcomeRequest(response) {
    var whichCityPrompt = "Which country would you like to send money?",
        speechOutput = {
            speech: "<speak>Welcome to paisa. A platform to know money transfer better. "
                + whichCityPrompt
                + "</speak>",
            type: AlexaSkill.speechOutputType.SSML
        },
        repromptOutput = {
            speech: "Sorry I don't have information about this country. "
                + whichCityPrompt,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };

    response.ask(speechOutput, repromptOutput);
}

function handleHelpRequest(response) {
    var repromptText = "Which city would you like tide information for?";
    var speechOutput = "I can lead you through providing a city and "
        + "day of the week to get tide information, "
        + "or you can simply open Tide Pooler and ask a question like, "
        + "get tide information for Seattle on Saturday. "
        + "For a list of supported cities, ask what cities are supported. "
        + "Or you can say exit. "
        + repromptText;

    response.ask(speechOutput, repromptText);
}

/**
 * Handles the case where the user asked or for, or is otherwise being with supported cities
 */
function handleSupportedCitiesRequest(intent, session, response) {
    // get city re-prompt
    var repromptText = "Which city would you like tide information for?";
    var speechOutput = "Currently, I know tide information for these coastal cities: " + getAllStationsText()
        + repromptText;

    response.ask(speechOutput, repromptText);
}

/**
 * Handles the dialog step where the user provides a city
 */

function handleCountryDialogRequest(intent, session, response) {

    var country = getCountryFromIntent(intent,false),
        repromptText,
        speechOutput;
    speechOutput = "To send money to "+ country.country +", here are the most popular money transfer services:  "
        + getAllmoneyAgents() 
        + "To help you find out the best service, tell me how fast you want your money to be sent? " ;
    repromptText = "Tell me how fast you want to send money. For e.g. you can say may be within a day or so. "
    
    response.ask(speechOutput, repromptText);
}

function handleOneTimeRequest(intent, session, response){
    var speechOutput;
    speechOutput = "Currently this facility is not available.";
    response.tellWithCard(speechOutput, "TidePooler", speechOutput)
}

/**
 * Handle no slots, or slot(s) with no values.
 * In the case of a dialog based skill with multiple slots,
 * when passed a slot with no value, we cannot have confidence
 * it is the correct slot type so we rely on session state to
 * determine the next turn in the dialog, and reprompt.
 */

function handleNoSlotDialogRequest(intent, session, response) {
    
        var repromptText = "Please try again saying a country name, for example, Canada. ";
        var speechOutput = repromptText;

        response.ask(speechOutput, repromptText);
}

function handleOnePaisaRequest(intent, session, response) {

     var country = getCountryFromIntent(intent,false),
        repromptText,
        speechOutput;
    if (country.error) {
        repromptText = "Currently, I information about these countries: " + getAllStationsText()
            + "Which country you want to send money ?";
        // if we received a value for the incorrect city, repeat it to the user, otherwise we received an empty slot
        speechOutput = country.countryCode ? "I'm sorry, I don't have any data for " + country.code
            + ". " + repromptText : repromptText;
        response.ask(speechOutput, repromptText);
        return;
    }
    
    // all slots filled, either from the user or by default values. Move to final request
    getFinalTideResponse(country, response);
}
/**
 * Both the one-shot and dialog based paths lead to this method to issue the request, and
 * respond to the user with the final answer.
 */
function getFinalTideResponse(cityStation, response) {

    // Issue the request, and respond to the user
    /*makeTideRequest(cityStation.station, date, function tideResponseCallback(err, highTideResponse) {
        var speechOutput;

        if (err) {
            speechOutput = "Sorry, the National Oceanic tide service is experiencing a problem. Please try again later";
        } else {
            // speechOutput = date.displayDate + " in " + cityStation.city + ", the first high tide will be around "
            //     + highTideResponse.firstHighTideTime + ", and will peak at about " + highTideResponse.firstHighTideHeight
            //     + ", followed by a low tide at around " + highTideResponse.lowTideTime
            //     + " that will be about " + highTideResponse.lowTideHeight
            //     + ". The second high tide will be around " + highTideResponse.secondHighTideTime
            //     + ", and will peak at about " + highTideResponse.secondHighTideHeight + ".";
            speechOutput = "We are still under development. No FX rates will be available now.";
        }

        response.tellWithCard(speechOutput, "TidePooler", speechOutput)
    });*/
    var speechOutput;
    speechOutput = "We are still under development.";
    response.tellWithCard(speechOutput, "TidePooler", speechOutput)
}

/**
 * Uses NOAA.gov API, documented: http://tidesandcurrents.noaa.gov/api/
 * Results can be verified at: http://tidesandcurrents.noaa.gov/noaatidepredictions/NOAATidesFacade.jsp?Stationid=[id]
 */
function makeTideRequest(station, date, tideResponseCallback) {

    var datum = "MLLW";
    var endpoint = 'http://tidesandcurrents.noaa.gov/api/datagetter';
    var queryString = '?' + date.requestDateParam;
    queryString += '&station=' + station;
    queryString += '&product=predictions&datum=' + datum + '&units=english&time_zone=lst_ldt&format=json';

    http.get(endpoint + queryString, function (res) {
        var noaaResponseString = '';
        console.log('Status Code: ' + res.statusCode);

        if (res.statusCode != 200) {
            tideResponseCallback(new Error("Non 200 Response"));
        }

        res.on('data', function (data) {
            noaaResponseString += data;
        });

        res.on('end', function () {
            var noaaResponseObject = JSON.parse(noaaResponseString);

            if (noaaResponseObject.error) {
                console.log("NOAA error: " + noaaResponseObj.error.message);
                tideResponseCallback(new Error(noaaResponseObj.error.message));
            } else {
                var highTide = findHighTide(noaaResponseObject);
                tideResponseCallback(null, highTide);
            }
        });
    }).on('error', function (e) {
        console.log("Communications error: " + e.message);
        tideResponseCallback(new Error(e.message));
    });
}

/**
 * Algorithm to find the 2 high tides for the day, the first of which is smaller and occurs
 * mid-day, the second of which is larger and typically in the evening
 */
function findHighTide(noaaResponseObj) {
    var predictions = noaaResponseObj.predictions;
    var lastPrediction;
    var firstHighTide, secondHighTide, lowTide;
    var firstTideDone = false;

    for (var i = 0; i < predictions.length; i++) {
        var prediction = predictions[i];

        if (!lastPrediction) {
            lastPrediction = prediction;
            continue;
        }

        if (isTideIncreasing(lastPrediction, prediction)) {
            if (!firstTideDone) {
                firstHighTide = prediction;
            } else {
                secondHighTide = prediction;
            }

        } else { // we're decreasing

            if (!firstTideDone && firstHighTide) {
                firstTideDone = true;
            } else if (secondHighTide) {
                break; // we're decreasing after have found 2nd tide. We're done.
            }

            if (firstTideDone) {
                lowTide = prediction;
            }
        }

        lastPrediction = prediction;
    }

    return {
        firstHighTideTime: alexaDateUtil.getFormattedTime(new Date(firstHighTide.t)),
        firstHighTideHeight: getFormattedHeight(firstHighTide.v),
        lowTideTime: alexaDateUtil.getFormattedTime(new Date(lowTide.t)),
        lowTideHeight: getFormattedHeight(lowTide.v),
        secondHighTideTime: alexaDateUtil.getFormattedTime(new Date(secondHighTide.t)),
        secondHighTideHeight: getFormattedHeight(secondHighTide.v)
    }
}

function isTideIncreasing(lastPrediction, currentPrediction) {
    return parseFloat(lastPrediction.v) < parseFloat(currentPrediction.v);
}

/**
 * Formats the height, rounding to the nearest 1/2 foot. e.g.
 * 4.354 -> "four and a half feet".
 */
function getFormattedHeight(height) {
    var isNegative = false;
    if (height < 0) {
        height = Math.abs(height);
        isNegative = true;
    }

    var remainder = height % 1;
    var feet, remainderText;

    if (remainder < 0.25) {
        remainderText = '';
        feet = Math.floor(height);
    } else if (remainder < 0.75) {
        remainderText = " and a half";
        feet = Math.floor(height);
    } else {
        remainderText = '';
        feet = Math.ceil(height);
    }

    if (isNegative) {
        feet *= -1;
    }

    var formattedHeight = feet + remainderText + " feet";
    return formattedHeight;
}

/**
 * Gets the city from the intent, or returns an error
 */
function getCityStationFromIntent(intent, assignDefault) {

    var citySlot = intent.slots.City;
    // slots can be missing, or slots can be provided but with empty value.
    // must test for both.
    if (!citySlot || !citySlot.value) {
        if (!assignDefault) {
            return {
                error: true
            }
        } else {
            // For sample skill, default to Seattle.
            return {
                city: 'seattle',
                station: STATIONS.seattle
            }
        }
    } else {
        // lookup the city. Sample skill uses well known mapping of a few known cities to station id.
        var cityName = citySlot.value;
        if (STATIONS[cityName.toLowerCase()]) {
            return {
                city: cityName,
                station: STATIONS[cityName.toLowerCase()]
            }
        } else {
            return {
                error: true,
                city: cityName
            }
        }
    }
}

    
function getCountryFromIntent(intent, assignDefault) {

    //var citySlot = intent.slots.City;
    var countrySlot = intent.slots.Country;
    
    // slots can be missing, or slots can be provided but with empty value.
    // must test for both.
    if (!countrySlot || !countrySlot.value) {
        if (!assignDefault) {
            return {
                error: true
            }
        } else {
            // For sample skill, default to Seattle.
            return {
                country: 'india',
                code: STATIONS.india
            }
        }
    } else {
        // lookup the city. Sample skill uses well known mapping of a few known cities to station id.
        var countryName = countrySlot.value;
        if (STATIONS[countryName.toLowerCase()]) {
            return {
                country: countryName,
                code: STATIONS[countryName.toLowerCase()]
            }
        } else {
            return {
                error: true,
                country: countryName
            }
        }
    }
}
/**
 * Gets the date from the intent, defaulting to today if none provided,
 * or returns an error
 */
function getDateFromIntent(intent) {

    var dateSlot = intent.slots.Date;
    // slots can be missing, or slots can be provided but with empty value.
    // must test for both.
    if (!dateSlot || !dateSlot.value) {
        // default to today
        return {
            displayDate: "Today",
            requestDateParam: "date=today"
        }
    } else {

        var date = new Date(dateSlot.value);

        // format the request date like YYYYMMDD
        var month = (date.getMonth() + 1);
        month = month < 10 ? '0' + month : month;
        var dayOfMonth = date.getDate();
        dayOfMonth = dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth;
        var requestDay = "begin_date=" + date.getFullYear() + month + dayOfMonth
            + "&range=24";

        return {
            displayDate: alexaDateUtil.getFormattedDate(date),
            requestDateParam: requestDay
        }
    }
}

function getAllStationsText() {
    var stationList = '';
    for (var station in STATIONS) {
        stationList += station + ", ";
    }

    return stationList;
}

function getAllmoneyAgents() {
    var stationList = '';
    moneyAgents.forEach(function(value){
        stationList += value + ", ";
    });

    return stationList;
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    var tidePooler = new TidePooler();
    tidePooler.execute(event, context);
};
